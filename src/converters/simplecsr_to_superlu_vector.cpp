/*
 * Copyright 2020 IFPEN-CEA
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * SPDX-License-Identifier: Apache-2.0
 */

#include <alien/core/backend/IVectorConverter.h>
#include <alien/core/backend/VectorConverterRegisterer.h>
#include <alien/kernels/simple_csr/SimpleCSRVector.h>
#include <alien/kernels/simple_csr/SimpleCSRBackEnd.h>

#include <alien/superlu/superlu_backend.h>
#include "../superlu_vector.h"

struct SimpleCSR_to_SuperLU_VectorConverter : public Alien::IVectorConverter
{
  SimpleCSR_to_SuperLU_VectorConverter() {}

  virtual ~SimpleCSR_to_SuperLU_VectorConverter() {}

  Alien::BackEndId sourceBackend() const { return Alien::AlgebraTraits<Alien::BackEnd::tag::simplecsr>::name(); }

  Alien::BackEndId targetBackend() const { return Alien::AlgebraTraits<Alien::BackEnd::tag::superlu>::name(); }

  void convert(const Alien::IVectorImpl* sourceImpl, Alien::IVectorImpl* targetImpl) const;
};

void SimpleCSR_to_SuperLU_VectorConverter::convert(const Alien::IVectorImpl* sourceImpl,
                                                   Alien::IVectorImpl* targetImpl) const
{
  const auto& v = cast<Alien::SimpleCSRVector<Arccore::Real>>(sourceImpl, sourceBackend());
  auto& v2 = cast<Alien::SuperLU::Vector>(targetImpl, targetBackend());

  alien_debug([&] {
    cout() << "Converting Alien::SimpleCSRVector: " << &v << " to SuperLU::Vector " << &v2;
  });
  auto values = v.values();
  v2.setValues(values);
}

REGISTER_VECTOR_CONVERTER(SimpleCSR_to_SuperLU_VectorConverter);
