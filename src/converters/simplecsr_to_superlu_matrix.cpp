/*
 * Copyright 2020 IFPEN-CEA
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * SPDX-License-Identifier: Apache-2.0
 */

#include <arccore/collections/Array2.h>

#include <alien/utils/Precomp.h>
#include <alien/core/backend/IMatrixConverter.h>
#include <alien/core/backend/MatrixConverterRegisterer.h>
#include <alien/kernels/simple_csr/SimpleCSRMatrix.h>
#include <alien/kernels/simple_csr/SimpleCSRBackEnd.h>

#include <alien/superlu/superlu_backend.h>
#include "../superlu_matrix.h"

struct SimpleCSR_to_SuperLU_MatrixConverter
: public Alien::IMatrixConverter
{
  SimpleCSR_to_SuperLU_MatrixConverter() {}

  virtual ~SimpleCSR_to_SuperLU_MatrixConverter() {}

  BackEndId sourceBackend() const
  {
    return Alien::AlgebraTraits<Alien::BackEnd::tag::simplecsr>::name();
  }

  BackEndId targetBackend() const { return Alien::AlgebraTraits<Alien::BackEnd::tag::superlu>::name(); }

  void convert(const Alien::IMatrixImpl* sourceImpl, Alien::IMatrixImpl* targetImpl) const;

  void _build(const Alien::SimpleCSRMatrix<Arccore::Real>& sourceImpl, Alien::SuperLU::Matrix& targetImpl) const;
};

void SimpleCSR_to_SuperLU_MatrixConverter::convert(
const IMatrixImpl* sourceImpl, IMatrixImpl* targetImpl) const
{
  const auto& v = cast<Alien::SimpleCSRMatrix<Arccore::Real>>(sourceImpl, sourceBackend());

  auto& v2 = cast<Alien::SuperLU::Matrix>(targetImpl, targetBackend());

  alien_debug([&] {
    cout() << "Converting Alien::SimpleCSRMatrix: " << &v << " to SuperLU::Matrix " << &v2;
  });
  if (targetImpl->block() || targetImpl->vblock())
    throw Arccore::FatalErrorException(A_FUNCINFO, "Block detected - builds not yet implemented");
  else
    _build(v, v2);
}

void SimpleCSR_to_SuperLU_MatrixConverter::_build(const Alien::SimpleCSRMatrix<Arccore::Real>& sourceImpl,
                                                  Alien::SuperLU::Matrix& targetImpl) const
{
  const auto& profile = sourceImpl.getCSRProfile();
  const auto localSize = profile.getNRow();
  const auto& matrixInternal = sourceImpl.internal();

  auto row_offset = profile.getRowOffset();
  auto cols = profile.getCols();
  auto values = matrixInternal.getValues();

  targetImpl.initMatrix(localSize,
                        row_offset,
                        cols,
                        values);
}

REGISTER_MATRIX_CONVERTER(SimpleCSR_to_SuperLU_MatrixConverter);
