/*
 * Copyright 2020 IFPEN-CEA
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * SPDX-License-Identifier: Apache-2.0
 */

#include "superlu_matrix.h"
#include <alien/superlu/superlu_backend.h>

#include <alien/core/impl/MultiMatrixImpl.h>
#include <alien/data/ISpace.h>

namespace Alien::SuperLU
{
Matrix::Matrix(const MultiMatrixImpl* multi_impl)
: IMatrixImpl(multi_impl, AlgebraTraits<BackEnd::tag::superlu>::name())
, m_local_size(0)
, m_pm(nullptr)
{
  const auto& row_space = multi_impl->rowSpace();
  const auto& col_space = multi_impl->colSpace();
  if (row_space.size() != col_space.size())
    throw Arccore::FatalErrorException("SuperLU matrix must be square");
  m_pm = multi_impl->distribution().parallelMng();
}

void Matrix::initMatrix(int local_size,
                        Arccore::ConstArrayView<int>& row_offset,
                        Arccore::ConstArrayView<int>& cols,
                        Arccore::ConstArrayView<double>& values)
{
  m_local_size = local_size;
  m_row_offset.copy(row_offset);
  m_cols.copy(cols);
  m_values.copy(values);
}

} // namespace Alien::SuperLU