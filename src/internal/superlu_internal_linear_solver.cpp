/*
 * Copyright 2020 IFPEN-CEA
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * SPDX-License-Identifier: Apache-2.0
 */

#include "superlu_internal_linear_solver.h"

#include <memory>

#include <slu_ddefs.h>

#include <alien/core/backend/LinearSolverT.h>

#include <alien/superlu/export.h>
#include <alien/superlu/superlu_backend.h>
#include <alien/superlu/superlu_options.h>
#include "../superlu_vector.h"
#include "../superlu_matrix.h"

namespace Alien
{
template class ALIEN_SUPERLU_EXPORT LinearSolver<BackEnd::tag::superlu>;

}

namespace Alien::SuperLU
{
InternalLinearSolver::InternalLinearSolver() {}

InternalLinearSolver::InternalLinearSolver(const Options& options)
: m_options(options)
{}

bool InternalLinearSolver::solve(const Matrix& A,
                                 const Vector& b,
                                 Vector& x)
{
  if (A.distribution().isParallel()) {
    alien_fatal([&]() {
      cout() << "solver superlu is not parallel";
    });
  }

  auto verbose = m_options.verbose() ? 1 : 0;

  /* Set the default input options. */
  superlu_options_t options;
  set_default_options(&options);
  options.ColPerm = NATURAL;

  auto m = A.rowSpace().size();
  auto n = A.colSpace().size();
  auto nnz = A.values().size();

  auto* a = const_cast<double*>(A.values().unguardedBasePointer());
  auto* asub = const_cast<int*>(A.cols().unguardedBasePointer());
  auto* xa = const_cast<int*>(A.rowOffset().unguardedBasePointer());

  /* Create matrix A in the format expected by SuperLU. */
  SuperMatrix superlu_A;
  dCreate_CompRow_Matrix(&superlu_A, m, n, nnz, a, asub, xa, SLU_NC, SLU_D, SLU_GE);

  auto Astore = static_cast<NCformat*>(superlu_A.Store);
  if (verbose) {
    alien_info([&] {
      printf("Dimension %dx%d; # nonzeros %d\n", superlu_A.nrow, superlu_A.ncol, Astore->nnz);
    });
  }

  // solve in place so rhs is copied in x vector
  auto view = b.values().constView();
  x.setValues(view);

  // now x is also rhs
  auto* rhs = const_cast<double*>(x.values().unguardedBasePointer());

  SuperMatrix superlu_B;
  dCreate_Dense_Matrix(&superlu_B, m, 1, rhs, m, SLU_DN, SLU_D, SLU_GE);

  int* perm_r = nullptr; /* row permutations from partial pivoting */
  int* perm_c = nullptr; /* column permutation vector */

  if (!(perm_r = intMalloc(m)))
    Arccore::FatalErrorException("Malloc fails for perm_r[].");
  if (!(perm_c = intMalloc(n)))
    Arccore::FatalErrorException("Malloc fails for perm_c[].");

  /* Initialize the statistics variables. */
  SuperLUStat_t stat;
  StatInit(&stat);

  SuperMatrix superlu_L; /* factor L */
  SuperMatrix superlu_U; /* factor U */

  auto info = -1;
  dgssv(&options, &superlu_A, perm_c, perm_r, &superlu_L, &superlu_U, &superlu_B, &stat, &info);

  if (info == 0) {
    m_status.succeeded = 0;

    if (verbose) {
      auto Lstore = static_cast<SCformat*>(superlu_L.Store);
      auto Ustore = static_cast<NCformat*>(superlu_U.Store);
      alien_info([&] {
        cout() << "No of nonzeros in factor L = " << Lstore->nnz;
        cout() << "No of nonzeros in factor U = " << Ustore->nnz;
        cout() << "No of nonzeros in L+U = " << (Lstore->nnz + Ustore->nnz - n);
        cout() << "FILL ratio = " << ((float)(Lstore->nnz + Ustore->nnz - n) / nnz);
      });
    }
  }
  else {
    m_status.succeeded = 1;

    if (verbose) {
      alien_info([&] {
        cout() << "dgssv() error returns INFO = " << info;
      });
    }
  }

  if (verbose) {
    alien_info([&] {
      if (info <= n) { /* factorization completes */
        mem_usage_t mem_usage;
        dQuerySpace(&superlu_L, &superlu_U, &mem_usage);
        cout() << "L\\U MB " << (mem_usage.for_lu / 1e6) << "\ttotal MB needed " << (mem_usage.total_needed / 1e6);
      }
    });
    StatPrint(&stat);
  }
  StatFree(&stat);

  SUPERLU_FREE(perm_r);
  SUPERLU_FREE(perm_c);
  Destroy_SuperMatrix_Store(&superlu_B);
  Destroy_SuperNode_Matrix(&superlu_L);
  Destroy_CompCol_Matrix(&superlu_U);

  return m_status.succeeded;
}

ALIEN_SUPERLU_EXPORT
IInternalLinearSolver<Matrix, Vector>*
InternalLinearSolverFactory(const Options& options)
{
  return new InternalLinearSolver(options);
}

ALIEN_SUPERLU_EXPORT
IInternalLinearSolver<Matrix, Vector>*
InternalLinearSolverFactory()
{
  return new InternalLinearSolver();
}

} // namespace Alien::SuperLU