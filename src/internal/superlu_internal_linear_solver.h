/*
 * Copyright 2020 IFPEN-CEA
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once

#include <alien/superlu/superlu_options.h>

#include <memory>

#include <alien/utils/Precomp.h>
#include <alien/expression/solver/SolverStater.h>
#include <alien/core/backend/IInternalLinearSolverT.h>
#include <alien/utils/ObjectWithTrace.h>

namespace Alien::SuperLU
{
class Matrix;

class Vector;

class InternalLinearSolver
: public IInternalLinearSolver<Matrix, Vector>
, public ObjectWithTrace
{
 public:
  typedef SolverStatus Status;

  InternalLinearSolver();

  InternalLinearSolver(const Options& options);

  virtual ~InternalLinearSolver() {}

 public:
  bool solve(const Matrix& A, const Vector& b, Vector& x);

  bool hasParallelSupport() const { return false; }

  const Status& getStatus() const { return m_status; }

  const SolverStat& getSolverStat() const { return m_stat; }

 private:
  Status m_status;
  SolverStat m_stat;

  Options m_options;
};

} // namespace Alien::SuperLU