/*
 * Copyright 2020 IFPEN-CEA
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once

#include <arccore/collections/Array.h>

#include <alien/core/impl/IMatrixImpl.h>

namespace Alien::SuperLU
{
class Matrix
: public IMatrixImpl
{
 public:
  Matrix(const MultiMatrixImpl* multi_impl);

  virtual ~Matrix() {}

 public:
  void clear() {}

  void initMatrix(int local_size,
                  Arccore::ConstArrayView<int>& row_offset,
                  Arccore::ConstArrayView<int>& cols,
                  Arccore::ConstArrayView<double>& values);

  auto& values() { return m_values; }
  const auto& values() const { return m_values; }

  auto& rowOffset() { return m_row_offset; }
  const auto& rowOffset() const { return m_row_offset; }

  auto& cols() { return m_cols; }
  const auto& cols() const { return m_cols; }

 private:
  int m_local_size;
  Arccore::UniqueArray<double> m_values;
  Arccore::UniqueArray<int> m_row_offset;
  Arccore::UniqueArray<int> m_cols;

  Arccore::MessagePassing::IMessagePassingMng* m_pm;
};

} // namespace Alien::SuperLU
