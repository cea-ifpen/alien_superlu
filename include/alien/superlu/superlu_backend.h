/*
 * Copyright 2020 IFPEN-CEA
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * SPDX-License-Identifier: Apache-2.0
 */

#pragma once

#include <alien/core/backend/BackEnd.h>

namespace Alien
{
template <class Matrix, class Vector>
class IInternalLinearSolver;

}

namespace Alien::SuperLU
{
class Matrix;
class Vector;
class Options;

extern IInternalLinearSolver<Matrix, Vector>* InternalLinearSolverFactory(const Options& options);
extern IInternalLinearSolver<Matrix, Vector>* InternalLinearSolverFactory();
} // namespace Alien::SuperLU

namespace Alien
{
namespace BackEnd::tag
{
  struct superlu
  {
  };
} // namespace BackEnd::tag

template <>
struct AlgebraTraits<BackEnd::tag::superlu>
{
  typedef SuperLU::Matrix matrix_type;
  typedef SuperLU::Vector vector_type;
  typedef SuperLU::Options options_type;
  typedef IInternalLinearSolver<matrix_type, vector_type> solver_type;

  static solver_type* solver_factory(const options_type& options)
  {
    return SuperLU::InternalLinearSolverFactory(options);
  }

  static solver_type* solver_factory()
  {
    return SuperLU::InternalLinearSolverFactory();
  }

  static BackEndId name() { return "superlu"; }
};

} // namespace Alien
